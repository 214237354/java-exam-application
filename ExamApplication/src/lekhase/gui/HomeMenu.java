/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lekhase.gui;

import lekhase.data.DataStorageException;
import lekhase.data.NotFoundException;
import lekhase.data.Student;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

/**
 *
 * @author JM Lekhase
 */
public class HomeMenu extends JFrame {

    JMenuItem miExit, miRegister, miShow, miSearch, miUpdate, miDelete;
    JMenu mnSubject, mnfile;
    private ArrayList<Student> arStud;

    public HomeMenu() {

        miRegister = new JMenuItem("Student Registration");
        miShow = new JMenuItem("View Student Records");
        miExit = new JMenuItem("Exit");
        miSearch = new JMenuItem("Search Student Record by Stud No");
        miUpdate = new JMenuItem("Update Student Phone");
        miDelete = new JMenuItem("Cancel Student Subject");
//listeners
        miRegister.addActionListener(new miRegisterEvent());
        miShow.addActionListener(new miShowEvent());
        miExit.addActionListener(new miExitEvent());
        miSearch.addActionListener(new miSearchEvent());
        miUpdate.addActionListener(new miUpdateEvent());
        miDelete.addActionListener(new miDeleteEvent());

        //creating menus
        mnfile = new JMenu("File");
        mnfile.add(miSearch);
        mnfile.add(miUpdate);
        mnfile.add(miDelete);
        mnfile.add(miExit);

        mnSubject = new JMenu("Students");
        mnSubject.add(miRegister);
        mnSubject.add(miShow);

        //create menu bar and add menus to menu bar
        JMenuBar jmBar = new JMenuBar();
        jmBar.add(mnfile);
        jmBar.add(mnSubject);

        //set menu bar
        setJMenuBar(jmBar);
        try {
            Student.initialise();
        } catch (DataStorageException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }

    }

    private class miRegisterEvent implements ActionListener {
//student Registration
        @Override
        public void actionPerformed(ActionEvent ae) {
            StudentRegistration frm = new StudentRegistration(arStud);
            frm.setTitle("STUDENT REGISTRATION FORM");
            frm.setSize(500, 300);
            frm.setResizable(false);
            frm.setVisible(true);
        }

    }

    private class miShowEvent implements ActionListener {
//view students records
        @Override
        public void actionPerformed(ActionEvent ae) {
            ViewStudentRecords frm = new ViewStudentRecords(arStud);
            frm.setTitle("VIEW STUDENTS RECORDS");
            frm.setSize(700, 300);
            frm.setResizable(false);
            frm.setVisible(true);
        }

    }

    private class miSearchEvent implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
//Searching using student no
            try {
                String search = JOptionPane.showInputDialog("Enter Stud No to Search:");
                Student ath = null;
                ath = Student.findStudRecord(search);
                JOptionPane.showMessageDialog(null, ath);
                System.out.println(ath);
            } catch (NotFoundException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());
            }
        }

    }

    private class miUpdateEvent implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            //update
            try {
                String search = JOptionPane.showInputDialog("Enter Stud No to Search:"); 
                Student ath = null;
                ath = Student.findStudRecord(search);
                String nsport = JOptionPane.showInputDialog("Enter New Phone:");
                ath.updatePhone(nsport);
            } catch (NotFoundException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());
            }
        }
    }

    private class miDeleteEvent implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            //DELETE
            try {
                String search = JOptionPane.showInputDialog("Enter Stud No to Search:");
                
                Student ath = null;
                ath = Student.findStudRecord(search);
                String subj = JOptionPane.showInputDialog("Enter Subjest Code:");
                ath.deleteSubject(subj);
            } catch (NotFoundException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());
            }

        }
    }

    private class miExitEvent implements ActionListener {
//closing the application
        @Override
        public void actionPerformed(ActionEvent ae) {
            try {
                int resp;
                resp = JOptionPane.showConfirmDialog(null, "Do u want to exit?", "Confirm", JOptionPane.YES_NO_OPTION);
                if (resp == JOptionPane.YES_OPTION) {
                    Student.terminate();
                    System.exit(0);
                } else {
                    mnSubject.requestFocus();
                }
            } catch (DataStorageException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());
            }
        }

    }

    public static void main(String[] args) {
      //Main Menu
        HomeMenu fMenu = new HomeMenu();
        fMenu.setSize(500, 500);
        fMenu.setTitle("HOME MENU");
        fMenu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        fMenu.setVisible(true);

    }

}
