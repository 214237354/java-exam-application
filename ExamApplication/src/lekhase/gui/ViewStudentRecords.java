/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lekhase.gui;

import lekhase.data.Student;
import java.awt.BorderLayout;
import java.awt.Container;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
/**
 *
 * @author JM Lekhase
 */
public class ViewStudentRecords extends JFrame{
    JTextArea taView=new JTextArea();
    
    public ViewStudentRecords(ArrayList<Student> arStud){
    taView.setEditable(false);
        Container scrn=getContentPane();
        
        scrn.setLayout(new BorderLayout());
        scrn.add(taView,BorderLayout.CENTER);
        displayAll(arStud);
       
    
    }
    
   public void displayAll(ArrayList<Student> arStud){
          String aSubj=JOptionPane.showInputDialog("Enter a Subject:");
         if(aSubj.isEmpty()){
              arStud=Student.getAll();

   
         }else{
          arStud=Student.getAllStudent(aSubj);
 
   
   }  
   taView.append("No of Loaded data: "+arStud.size()+"\n");
   taView.append("Student No\t"+"Student Name\t"+"Cell Number\t"+"Gender \t"+"Course Code\t" +"Subject Code\t"+"\n");
   for(int x=0;x<arStud.size();x++){
    taView.append(arStud.get(x).toString()+"\n");
   
   
   }
}
}
